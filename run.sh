#!/bin/bash
set -e


gcc -o main main.c -I/opt/raylib/src -L/opt/raylib/src -lraylib -lm -lpthread -ldl -ggdb
./main
