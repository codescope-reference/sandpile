#include "raylib.h"
#include <stdlib.h>
#include <stdbool.h>

#define RECT_IMPLEMENTATION
#include "rect.h"


#define STB_DS_IMPLEMENTATION
#include "stb_ds.h"

#define W 800
#define H 800

static int updates_per_step = 1;


typedef struct {
    int key;
} KVP;

KVP *hm = NULL;

typedef struct {
    int width;
    int height;

    int *grains;


    bool draw_borders;
} Sandpile;


Color palette[] = {
    BLACK,
    DARKBLUE,
    VIOLET,
    LIME,
    RED,
};


void draw_sandpile(Rectangle location, Sandpile *pile) {
    float cell_width = location.width / pile->width;
    float cell_height = location.height / pile->height;

    for (int row = 0; row < pile->height; row++) {
        for (int col = 0; col < pile->width; col++) {
            Rectangle cell = {
                location.x + col * cell_width,
                location.y + row * cell_height,
                cell_width,
                cell_height
            };

            int grains = pile->grains[row * pile->width + col];
            if (grains >= 4) {
                grains = 4;
            }
            Color color = palette[grains];
            DrawRectangleRec(cell, color);
            if (pile->draw_borders) {
                DrawRectangleLinesEx(cell, 1.0f, RAYWHITE);
            }

            Vector2 mouse_pos = GetMousePosition();
            if (CheckCollisionPointRec(mouse_pos, cell)) {
                if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
                    pile->grains[row * pile->width + col] += 1;
                    if (pile->grains[row * pile->width + col] >= 4) {
                        hmputs(hm, (KVP) {row * pile->width + col});
                    }
                }
            }
        }
    }
}


void update_pile(Sandpile *pile) {
    if (hmlen(hm) == 0) {
        return;
    }
    int hm_index = rand() % hmlen(hm);

    int update_index = hm[hm_index].key;

    pile->grains[update_index] -= 4;
    if (pile->grains[update_index] < 4) {
        hmdel(hm, update_index);
    }

    int row = update_index / pile->width;
    int col = update_index % pile->width;

    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dx == 0 && dy == 0) continue;
            if (dx != 0 && dy != 0) continue;

            int new_row = row + dy;
            int new_col = col + dx;
            if (new_row < 0 || new_row >= pile->height) continue;
            if (new_col < 0 || new_col >= pile->width) continue;

            int new_index = new_row * pile->width + new_col;
            pile->grains[new_index] += 1;
            if (pile->grains[new_index] >= 4) {
                hmputs(hm, (KVP) {new_index});
            }
        }
    }
}


int main(void) {
    InitWindow(W, H, "Sandpiles");
    SetTargetFPS(120);

    Rectangle window = {0, 0, W, H};
    Rectangle pile_grid = rect_center(window, 0.9, 0.9);

    int size = 100;
    Sandpile pile = {
        .width = size,
        .height = size,
        .grains = (int *)calloc(size * size, sizeof(int)),
        .draw_borders = false
    };
    pile.grains[size * size / 2 + size / 2] = 10000;
    hmputs(hm, (KVP) {size * size / 2 + size / 2});

    float time_since_last_update = 0.0f;
    float update_interval = 0.0f;
    while (!WindowShouldClose()) {
        BeginDrawing();
        ClearBackground((Color){51, 51, 51, 255});
        DrawFPS(10, 10);
        DrawText(TextFormat("Updates per step: %d", updates_per_step), 150, 10, 20, RAYWHITE);
        DrawText(TextFormat("Grains at center: %d", pile.grains[size * size / 2 + size / 2]), 450, 10, 20, RAYWHITE);

        draw_sandpile(pile_grid, &pile);

        time_since_last_update += GetFrameTime();
        if (time_since_last_update >= update_interval) {
            time_since_last_update = 0.0f;
            for (int i = 0; i < updates_per_step; i++) {
                update_pile(&pile);
            }
        }



        EndDrawing();


        if (IsKeyDown(KEY_TWO)) {
            updates_per_step += 1;
        } 
        if (IsKeyDown(KEY_ONE)) {
            updates_per_step -= 1;
            if (updates_per_step < 0) {
                updates_per_step = 0;
            }
        } 
    }

    CloseWindow();
    return 0;
}
